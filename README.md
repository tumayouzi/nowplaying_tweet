# last.fmからNowplaying取得してツイートする奴

## 概要
指定したユーザの再生情報を取得し、twitterにツイート出来るTweet Web IntentのURLを生成して開きます。  


## 使用上の注意
このプログラムの使用により発生したいかなる問題に対しても作者は責任を持てません。  
各使用者の自己責任において使用してください。  


## 必要環境
* Windows 7以降のPC  
* Python 3.5以降(2.系では動作しません）  
* 少しの英語能力とテキストエディタでコードを弄くれる力  


## 準備する物
* last .fm Scrobbler <http://www.last.fm/ja/about/trackmymusic>  
WindowsでScrobblerに対応するオーディオプレイヤーが必要です。  
* 適当な好みのブラウザ  


## 初期設定
###設定変数の変更  
* browser_custom_flag  
標準でInternet ExprolerあるいはEdgeを使用している方は0、chromeやfirefox、Operaを使用してる方は1にしてください。  

* tgt_browser_path
browser_custom_flagを1に変更した場合は、使用しているブラウザのフルパスを_ダブルクォーテーションで囲って_記述してください。  
_また、ディレクトリ区切りの\は全て一つ付け足し、\\にしてください。_  
#### example
* 編集前のパス  
    `"C:\Program Files (x86)\Mozilla Firefox\firefox.exe"`
* 編集後のパス  
    `"C:\\Program Files (x86)\\Mozilla Firefox\\firefox.exe"`  
ブラウザのパスはショートカットを右クリック、プロパティを開いて表示されるリンク先に記述されています。  
デフォルトでは64bit Windowsに標準でインストールされるfirefoxのパスになっています。  
また、サンプルは変数の下に記述しています。  

* lastfm_APIkey  
<http://www.last.fm/api/account/create>からlast.fmのAPIKeyを取得します。  
Contact emailとApplication nameの入力のみでもKeyを取得出来ます。  
取得後、Keyをlastfm_APIkeyに設定してください。  

* lastfm_user  
last.fmのユーザ名を入力してください。  

## 使い方
nowplaying.pyをダブルクリックするか、ダブルクリックで動かない場合は以下のバッチファイルを作成し、使いやすい場所にショートカットを作成してください。
バッチファイルはpythonにpthが通っていることが前提です。  
    `@echo off`  
    `python nowplaying.py`  


## デバッグ環境  
Windows10 64bit RedStone 1  
Python3.5.1  
firefox 48.0.2  

## 更新履歴
* Ver 0.0.0.1 - First Release.  
* Ver 0.0.0.2 - 取得したjsonでNowPlayingの状態がtrueになっていない場合はツイートしない様に変更し、メッセージを表示する様にした。
