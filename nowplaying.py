#!/usr/bin/env python
#-*- coding:utf-8 -*-

import urllib.request
import urllib.parse
import webbrowser
import json
import sys
import os


#### config ####

## ブラウザの設定
browser_custom_flag = 0 #tweetするブラウザを変更する場合は1、IE/edgeの場合は0にしてください。
tgt_browser_path = '"C:\\Program Files (x86)\\Mozilla Firefox\\firefox.exe"' #tweetするブラウザの設定。フルパスで入力する。
#ブラウザパスの参考
#  32bit OS
#    firefox C:\\Program Files\\Mozilla Firefox\\firefox.exe
#    chrome C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe
#  64bit OS
#    firefox C:\\Program Files (x86)\\Mozilla Firefox\\firefox.exe
#    chrome C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe
## ブラウザの設定　ここまで

## last.fmの設定
lastfm_APIkey = '' #APIキーは以下のURLから取得してください。
#APIキー取得　URL　http://www.last.fm/api/account/create
lastfm_user = '' #自身のlast.fmのログインユーザIDに変更してください。
## last.fmの設定　ここまで

#### config End ####


#### variable init ####

json_data = '' #jsonデータ格納用
nowplaying_flag = False #nowplaying状態か判断する

#### init End ####



def JsonGet():
    global json_data
    url = 'http://ws.audioscrobbler.com/2.0/?method=user.getrecenttracks&user=' + lastfm_user + '&limit=1&api_key=' + lastfm_APIkey + '&format=json' #jsonを取得してくるURL

    urlread = urllib.request.urlopen(url)
    json_data = urlread.read()
    urlread.close()

def JsonLoad():
    global json_data
    global nowplaying_flag

    json_data = json_data.decode('utf-8')

    nowplaying_flag = '"nowplaying":"true"' in json_data
    json_data = json.loads(json_data)

def JsonTweet():
    if (nowplaying_flag == True): #nowPlayingしていればTweetを行う
        #各変数にjson_dataから情報を取得して格納する
        now_album = json_data['recenttracks']['track'][0]['album']['#text']
        now_artist = json_data['recenttracks']['track'][0]['artist']['#text']
        now_name = json_data['recenttracks']['track'][0]['name']
        now_art = json_data['recenttracks']['track'][0]['image'][3]['#text']

        #なうぷれデータをコンソールに気分的に出力
        print('Now Playing: ' + now_name + '(Album:' + now_album + ')/' + now_artist + ' ' + now_art)

        #URLの生成 生成フォーマットはここから変更する
        tweet_url = 'Now Playing: ' + now_name + ' - ' + now_artist + ' (Album:' + now_album + ') ' + now_art + ' #nowplaying'
        tweet_url = urllib.parse.quote(tweet_url, '') #パーセントエンコーディングしないと化けたりツイート内容生成が上手くいかない
        tweet_url = 'https://twitter.com/intent/tweet?text=' + tweet_url #ツイート用のURLを後から付け足す

        #ツイート用ブラウザの分岐
        if browser_custom_flag == 0 : #IE/edgeが開く
            webbrowser.open(tweet_url)
        elif browser_custom_flag == 1 : #ユーザが指定したブラウザで開く
            browser = webbrowser.get(tgt_browser_path + ' %s')
            browser.open(tweet_url)
    else:
        print ('NowPlayingしている曲がないよ！')

if __name__ == '__main__':
	JsonGet()
	JsonLoad()
	JsonTweet()
